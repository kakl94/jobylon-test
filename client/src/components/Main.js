import React, { Component } from 'react';
import { Container } from 'reactstrap';
import Content from './Content';
import Header from './Header';

import './main.css';

const API = 'https://feed.jobylon.com/feeds/7d7e6fd12c614aa5af3624b06f7a74b8/?format=json';

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      jobs: []
    }
  }

  // fetch data from API
  componentWillMount() {
    fetch(API)
      .then(res => res.json())
      .then(res => this.setState({ jobs: res }))
  }

  render() {
    return (
      <Container className="main-container">
        <Header />
        <Content jobs={this.state.jobs} />
      </Container>
    );
  }
}

export default Main;
