import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMapMarkerAlt, faBuilding, faBusinessTime } from '@fortawesome/free-solid-svg-icons'

import './main.css';

library.add([faMapMarkerAlt, faBuilding, faBusinessTime]);

class Content extends Component {
    render() {
        return (
            <div className="content-div">
                <h4 className="result-txt"><span className="result-int">{this.props.jobs.length}</span> jobs found</h4>
                {this.props.jobs.map(job =>
                    <div key={job.id}>
                        <div className="job-box">
                            <img src={job.company.logo} className="company-logo" alt="" />
                            <Col className="job-col">
                                <Row className="job-title">
                                    <h2>{job.title}</h2>
                                </Row>
                                <Row className="job-comp">
                                    <h4>
                                        <FontAwesomeIcon icon="building" className="icon" />
                                        {job.company.name}
                                    </h4>
                                </Row>
                                <Row className="job-loc">
                                    <h4>
                                        <FontAwesomeIcon icon="map-marker-alt" className="icon" />
                                        {job.locations[0].location.text}
                                    </h4>
                                </Row>
                                <Row className="job-empl-type">
                                    <h5>
                                        <FontAwesomeIcon icon="business-time" className="icon" />
                                        {job.employment_type}
                                    </h5>
                                </Row>
                            </Col>
                            <div className="btn-div">
                                <a href={job.urls.ad} target="_blank" rel="noopener noreferrer">
                                    <button className="btn">READ MORE & APPLY</button>
                                </a>
                            </div>
                        </div>
                    </div>
                )}
            </div>
        );
    }
}

export default Content;
