import React, { Component } from 'react';

import './main.css';

class Header extends Component {
    render() {
        return (
            <div className="header-div">
                <h1>Jobs</h1>
            </div>
        );
    }
}

export default Header;